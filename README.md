# let's encrypt

## Automatisation full avec invoke

Lister les taches: `inv -l`

Detail des taches:

```
invoke publish
invoke certificate
inv certificate --domaine=pasgrave.net
invoke apachectl
invoke clean
invoke validity pasgrave.net:443
invoke validity radicale.pasgrave.net
```

Tache all: execute all tasks: pub, cert all domaines, apachectl, clean, validity


## Tester le script update_certificate.py manuellement

Mode opératoire (depuis pollux)

- Copier le code:

```sh
rsync -av update_certificate.py gandi_key gandi_http.py kimsuffi:/home/srs/certificate/
```

- Lancer la commande:

```sh
python3 update_certificate.py pasgrave.net 1
sudo apachectl restart
rm -rf *
```

## Tester l'API gandi avec curl (en utilisant update_record_dns.sh)

```sh
./update_record_dns.sh NOM_DU_RECORD_DNS CLE_GANDI
```


## Commande certbot-auto

### Lister les certificats

```sh
certbot-auto certificates
```

### Revoquer un certificat

```sh
certbot-auto revoke --cert-name www.pasgrave.net
```
