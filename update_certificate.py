#!/usr/bin/env python3

from subprocess import Popen, PIPE
import re
import gandi_http
import sys

# Exemple d'utilisation
# update_certificate.py pasgrave.net True

# Ex de domaine: 
# - pasgrave.net
# - *.pasgrave.net
DOMAIN=sys.argv[1]

# Mode test: True | False, 1|0 ....
dry_run = "" 
# if len(sys.argv) == 3 and sys.argv[2]:
#   dry_run = "--dry-run" 

# Record pour un sous domaine est de la forme "_acme-challenge.SOUS_DOMAIN"
DNS_RECORD="_acme-challenge"

# Commande certbot
command="/usr/local/bin/certbot-auto -d %s --manual --preferred-challenges dns certonly %s" % (DOMAIN, dry_run)

# Log parametres
print("Domaine: %s, Dry-run: %s" % (DOMAIN, dry_run))
print("Commande: %s" % (command))

# lancement de popen
p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, bufsize=0, universal_newlines=True)

# le challenge est une ligne (début non vide, finissant par un retour chariot)
# de 43 caractères
challenge_pattern=re.compile(r"^([^:\s]{43})\n$")

out = p.stdout.readline()
while out:
  line = out
  match_challenge = challenge_pattern.match(line)
  if "Are you OK with your IP being logged" in line:
      p.stdin.write('Y\n')
      out = p.stdout.readline()
      continue
  elif match_challenge:
      certkey = match_challenge.group(1) 
      # update DNS
      response = gandi_http.put_txt_record(DNS_RECORD, certkey)
      if response.ok:
        print("Successful update DNS record %s with value %s" % (DNS_RECORD, certkey))
      else:
        print("Fail to save DNS record %s with value %s" % (DNS_RECORD, certkey))

      p.stdin.write('\n')
      out = p.stdout.readline()
      continue
  else:
      print("++%s" % line.rstrip("\n"))
      out = p.stdout.readline()
      continue

p.wait()
