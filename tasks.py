#!/usr/bin/env python3

import os
import shutil
from invoke import task
from invoke.tasks import call
from fabric import Connection

run = {
    "echo": False
}
CONFIG = {
    'host': 'srs@pasgrave.net',
    'ssh_port': '1969',
    'dest': '/home/srs/certificate',
    'cert_command': './update_certificate.py',
    'domaine': '*.pasgrave.net',
    'dry_run': 'False'
}

@task
def clean(c):
    """Clean remote files"""
    with Connection('{host}:{ssh_port}'.format(**CONFIG)) as conn:
        conn.run('rm -rf {dest}'.format(**CONFIG))
        conn.run('mkdir {dest}'.format(**CONFIG))

@task
def publish(c):
    """Publish to remote via rsync"""
    c.run(
        'rsync --delete -av '
        '* {host}:{dest} -e "ssh -p {ssh_port}"'.format(**CONFIG))

@task
def certificate(c, domaine=CONFIG['domaine']):
    """launch certbot on remote host"""
    CONFIG['domaine'] = domaine
    with Connection('{host}:{ssh_port}'.format(**CONFIG)) as conn:
        conn.run('cd {dest} && sudo {cert_command} {domaine} {dry_run}'.format(**CONFIG))
        
@task
def apachectl(c):
    """Restart apache"""
    with Connection('{host}:{ssh_port}'.format(**CONFIG)) as conn:
        conn.run('sudo apachectl restart')

@task
def validity(c, domaine):
    """Test certificate validity"""
    c.run('echo "" | openssl s_client -connect {} -prexit 2>/dev/null | openssl x509 -text | grep "Not After"'.format(domaine))

@task(publish, call(certificate, '*.pasgrave.net'), apachectl, clean, call(validity, 'pasgrave.net:443'))
def all(c, default=True):
    """Execute all tasks: pub, cert all domaines, apachectl, clean, validity"""
    print("all")
