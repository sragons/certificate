import requests

my_gandi_key=open("gandi_key", "r").read()

api_url="https://api.gandi.net/v5/livedns/domains/pasgrave.net/records/{}/TXT"

def put_txt_record(name, value):
  url=api_url.format(name)
  print(url)
  data={"rrset_values":[value]}
  print(data)
  headers = {'Content-Type': 'application/json', 'Authorization': my_gandi_key}
  print(headers)
  return requests.put(url, json=data, headers=headers)

if __name__ == '__main__':
  put_txt_record("_acme-challenge.test", "test")