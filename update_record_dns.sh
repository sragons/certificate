#!/bin/bash

dns_record=$1 
# dns_record=_acme-challenge
gandi_key=$2

authentication="Authorization: Apikey ${gandi_key}"
api_url=https://api.gandi.net/v5/livedns/domains/pasgrave.net/records/$dns_record/TXT


# Ask the user for the value
echo valeur de la clé DNS
read value

sed "s/__val__/$value/" pasgrave.template > pasgrave.json
curl -H 'Content-Type: application/json' -H "$authentication" $api_url -X PUT -d @pasgrave.json


